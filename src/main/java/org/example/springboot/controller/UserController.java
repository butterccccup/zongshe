package org.example.springboot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.example.springboot.Exceptions.ParamsException;
import org.example.springboot.Util.SessionUtils;
import org.example.springboot.po.Door;
import org.example.springboot.po.State;
import org.example.springboot.po.User;
import org.example.springboot.service.DoorService;
import org.example.springboot.service.StateService;
import org.example.springboot.service.UserService;
import org.example.springboot.service.MessageService;
import org.example.springboot.service.WebSocketServer;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private StateService stateService;
    @Resource
    private DoorService doorService;

    //用户注册
    @PostMapping("user/register")
    @ResponseBody
    public Map<String,Object> addUser(@RequestBody User user){
        Map<String,Object> map = new HashMap<>();
        try{
            QueryWrapper<Door> wrapper = new QueryWrapper<>();
            wrapper.eq("dunit",user.getUunit());
            Door door = doorService.getOne(wrapper);
            user.setDid(door.getDid());
            userService.save(user);
            map.put("code",200);
            map.put("msg","添加用户成功");
        }catch(ParamsException p){
            //自定义
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
            p.printStackTrace();
        }catch(Exception e){
            //代码问题
            map.put("code",500);
            map.put("msg","添加用户失败！");
            e.printStackTrace();
        }
        return map;
    }
    @PutMapping("user/update")
    @ResponseBody
    public Map<String,Object> updateUser (@RequestBody User user){
        Map<String,Object> map = new HashMap<>();
        User userTmp = userService.getById(user.getUid());
        try{
            if(userTmp!=null&&userTmp.getUtel().equals(user.getUtel())) {
                userTmp.setUpassword(user.getUpassword());
                userService.updateById(userTmp);
                map.put("code", 200);
                map.put("msg", "修改密码成功!");
            }
        } catch (ParamsException p){
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
        } catch (Exception e){
            map.put("code",500);
            map.put("msg","修改密码失败!");
            e.printStackTrace();
        }

        return map;
    }

    @DeleteMapping("user/admin/delete/{uId}")
    @ResponseBody
    public Map<String,Object> deleteUser(@PathVariable String uId){
        Map<String,Object> map = new HashMap<>();
        /*User user = SessionUtils.getCurrentUserInfo();
        if(!user.getAdmin()){
            map.put("code",500);
            map.put("msg","删除用户失败");
            return map;
        }*/

        try{
            userService.removeById(uId);
            map.put("code",200);
            map.put("msg","删除用户成功!");
        }catch(ParamsException p){
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
            p.printStackTrace();
        }catch(Exception e){
            map.put("code",500);
            map.put("msg","删除用户失败!");
            e.printStackTrace();
        }
        return map;
    }
    //用户登录
    @RequestMapping(value = "user/login", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> login(@RequestBody User user) throws Exception{
        User userTmp = userService.getById(user.getUid());
        Map<String,Object> map = new HashMap<>();
        try{
            if(userTmp!=null && user.getUpassword().equals(user.getUpassword()))
            map.put("code",200);
            map.put("msg","登录成功!");
        }catch(ParamsException p){
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
            p.printStackTrace();
        }catch(Exception e){
            map.put("code",500);
            map.put("msg","登录失败!");
            e.printStackTrace();
        }
        SessionUtils.saveCurrentUserInfo(user);         //session保存当前用户
        return map;
    }




    //开门测试
    @PostMapping("user/kai")
    @ResponseBody
    public Map<String,Object> kai(){

        Map<String,Object> map = new HashMap<>();
        Date date=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String TIME=sdf.format(date);                 //时间日期

        MessageService msg = new MessageService().formMessage("9887549278","instruction",TIME,"{\"description\":\"open\"}"); //开门JSON;
        try {
            WebSocketServer.sendInfo(msg.toString(),"9887549278");
            map.put("code",200);
            map.put("msg","开门成功!");
        } catch (IOException var4) {
            var4.printStackTrace();
            map.put("code",500);
            map.put("msg","开门失败");
        }
        return map;
    }
    //开门
    @PostMapping("user/open")
    @ResponseBody
    public Map<String,Object> open(){
        Map<String,Object> map = new HashMap<>();

        User user = SessionUtils.getCurrentUserInfo();  //从Session获取当前用户实体
        Date date=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String TIME=sdf.format(date);                 //时间日期

        MessageService msg = new MessageService().formMessage(user.getDid(),"instruction",TIME,"{'description':'pin on'}"); //开门JSON;
        try {
            WebSocketServer.sendInfo(msg.toString(),user.getDid());
            map.put("code",200);
            map.put("msg","开门成功!");
        } catch (IOException var4) {
            var4.printStackTrace();
            map.put("code",500);
            map.put("msg","开门失败");
        }
        return map;
    }
    //查看门状态
    @GetMapping("user/check")
    @ResponseBody
    public Map<String,Object> check() throws IOException {
        Map<String,Object> map = new HashMap<>();

        Date date=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String TIME=sdf.format(date);                 //时间日期

        User user = SessionUtils.getCurrentUserInfo();  //从Session获取当前用户实体
        MessageService msg = new MessageService().formMessage(user.getDid(),"instruction",TIME,"{'description':'state request'}"); //向门请求状态
        WebSocketServer.sendInfo(msg.toString(),user.getDid());

        QueryWrapper<State> wrapper = new QueryWrapper<>();
        wrapper.ge("sTime",TIME);       //找一个比刚才发送时间(TIME)更大(或者相等)的时间，这个时间对应着刚才门发过来的状态时间
        State state = stateService.getOne(wrapper);
        map.put("code",200);
        map.put("CPU温度",state.getStemperature());
        map.put("开关状态",state.getSpin());
        return map;
    }







}
