package org.example.springboot.controller;

import org.example.springboot.Exceptions.ParamsException;
import org.example.springboot.Util.SessionUtils;
import org.example.springboot.dao.DoorMapper;
import org.example.springboot.po.Door;
import org.example.springboot.po.User;
import org.example.springboot.service.DoorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

//RestController = Controller + ResponseBody;但不能返回HTML视图了，要用的话自己查一下
@RestController
public class DoorController {
    @Resource
    private DoorService doorService;
    @Resource
    private DoorMapper doorMapper;

/*
    //@GetMapping("Door/name/{userName}")
    //@ResponseBody
   // public Door queryByUserName(@PathVariable String userName){
        return UserService.queryByUserName(userName);
    }
*/
    /*@GetMapping("user/{dId}")
    public Door queryByUserID(@PathVariable String dId){
        return doorService.queryByUserID(dId);
    }*/

    @PostMapping("door/admin/add/{dunit}")
    @ResponseBody
    public Map<String,Object> addDoor(@PathVariable String dunit){
        Map<String,Object> map = new HashMap<>();
        Door door = new Door();

        String id = "";
        String password = "";

        Random random = new Random();
        List<Door> list = doorService.list();

        for(int i = 0;i<10;i++){
            id = id + ( char)((int)(Math.random()*('9'-'0'+1)+'0'));
        }
        for (Door tmp : list){

            if(tmp.getDid().equals(id))
                for(int i = 0;i<10;i++){
                    id = id + (char)((int)(Math.random()*('9'-'0'+1)+'0'));
                }
        }
        door.setDid(id);

        //生成强密码
        for(int i = 0;i<16;i++){
            int r = (int)(Math.random()*3);
            if(r == 0){
                password = password + (char)((int)(Math.random()*('z'-'a'+1)+'a'));
            }else  if(r == 1){
                password = password + (char)((int)(Math.random()*('Z'-'A'+1)+'A'));
            }else if(r == 2){
                password = password + (char)((int)(Math.random()*('9'-'0'+1)+'0'));
            }
        }
        door.setDpassword(password);

        door.setDunit(dunit);
        door.setIfregistered(false);
        try{

            doorService.save(door);
            map.put("code",200);
            map.put("msg","add Door successfully!");
        }
        catch (ParamsException p){
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
        }
        catch (Exception e){
            map.put("code",500);
            map.put("msg","add Door unsuccessfully!");
            System.out.println(e.getMessage());
        }
        return map;
    }

    @DeleteMapping("/door/admin/delete/{did}")
    @ResponseBody
    public Map<String,Object> deleteDoor(@PathVariable String did){
        Map<String,Object> map = new HashMap<>();
        try{
            doorService.removeById(did);
            map.put("code",200);
            map.put("msg","删除设备成功!");
        }catch(ParamsException p){
            map.put("code",p.getCode());
            map.put("msg",p.getMsg());
            p.printStackTrace();
        }catch(Exception e){
            map.put("code",500);
            map.put("msg","删除设备失败!");
            e.printStackTrace();
        }
        return map;
    }

    @GetMapping("door/admin/list")
    @ResponseBody
    public List<Door> list(){
        List<Door> list = doorService.list();
        return list;
    }

}
