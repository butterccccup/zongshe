
package org.example.springboot.controller;

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import org.example.springboot.service.WebSocketServer;
import java.io.IOException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebSocketController {
    public WebSocketController() {
    }

    @GetMapping({"/socket/{cid}"})
    public ModelAndView socket(@PathVariable String cid) {
        ModelAndView mav = new ModelAndView("/socket");
        mav.addObject("cid", cid);
        return mav;
    }
    String m = "hello";
    @ResponseBody
    @RequestMapping({"/socket/push/{cid}"})
    public String pushToWeb(@PathVariable String cid, String message) {
        try {
            WebSocketServer.sendInfo(m, cid);
            return "发送成功";
        } catch (IOException var4) {
            var4.printStackTrace();
            return "推送失败";
        }
    }
}

