package org.example.springboot.Util;

public class stateCONTENT {
    String temperature;
    String pin;

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public stateCONTENT(String temperature,String pin){
        this.temperature = temperature;
        this.pin = pin;
    }
}
