package org.example.springboot.Util;

import org.example.springboot.Exceptions.ParamsException;

public class AssertUtil {
    public static void isTrue(boolean flag,String msg){
        if(flag){
            throw new ParamsException(msg);
        }
    }
}
