package org.example.springboot.service;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.alibaba.fastjson.JSON;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import org.springframework.stereotype.Component;


@ServerEndpoint("/websocket/log/{sid}")
@Component
public class WebSocketServer {
    public static MessageService M;
    static Log log = LogFactory.getLog(WebSocketServer.class);
    private static int onlineCount = 0;
    private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet();
    private Session session;
    private String sid = "";
    private boolean flag = false;

    public WebSocketServer() {
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("sid") String sid) {
        this.session = session;
        webSocketSet.add(this);
        addOnlineCount();
        log.info("有新窗口开始监听:" + sid + ",当前在线人数为" + getOnlineCount());
        this.sid = sid;
        try {
            //this.sendMessage("连接成功");
            //this.sendMessage("JSON");
            System.out.println("树莓派连接建立！！！");
        } catch (Exception var4) {
            log.error("websocket IO异常");
        }
    }
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        subOnlineCount();
        log.info("有一连接关闭！当前在线人数为" + getOnlineCount());
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {

        log.info("收到来自窗口" + this.sid + "的信息:" + message);
        /*if(!flag){

        }else {
            sendMessage(M.recvMessage(message).toString());
        }*/
        MessageService msgRecv = JSON.parseObject(message,MessageService.class);
        MessageService msg = M.recvMessage(message);
        if(msg.getCONTENT().equals("{\"description\":\"解析失败\"}"))
            session.close();

        if(msg.getCONTENT().equals("{\"description\":\"logon unsuccessfully\"}")||
                msg.getCONTENT().equals("{\"description\":\"register unsuccessfully\"}")){
            sendMessage(msg.toString());
            session.close();
        }else{
            if(msgRecv.getTYPE().equals("state")||msgRecv.getTYPE().equals("reply"))
                return;
            sendMessage(msg.toString());
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    public static void sendInfo(String message, @PathParam("uid") String uid) throws IOException {
        log.info("推送消息到窗口" + uid + "，推送内容:" + message);

        for (WebSocketServer item : webSocketSet) {
            try {
                if (uid == null) {
                    item.sendMessage(message);
                } else if (item.sid.equals(uid)) {
                    item.sendMessage(message);
                }
            } catch (IOException var5) {
            }
        }

    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        ++onlineCount;
    }

    public static synchronized void subOnlineCount() {
        --onlineCount;
    }
}

