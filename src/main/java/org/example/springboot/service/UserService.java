package org.example.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.commons.lang3.StringUtils;
import org.example.springboot.Util.AssertUtil;
import org.example.springboot.dao.UserMapper;
import org.example.springboot.po.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


public interface UserService extends IService<User> {

   /* @Resource
    private UserMapper userMapper;

    //开门
    public void openDoor(String dId){

    }

    //添加用户
    public void addUser(User user){
        //写法二： 不带返回值，只抛出异常
        AssertUtil.isTrue(StringUtils.isBlank(user.getuPassword()),"用户密码不能为空！");
        AssertUtil.isTrue(StringUtils.isBlank(user.getdId()),"用户ID不能为空！");
        //isBlank方法判断字符串是否为空，会去除前后空格；
        //若返回true，则会将错误信息返回给AssertUtil，抛出异常
        //抛出的异常会在controller层被捕获，所以controller层需要一个try..catch

        User temp = userMapper.getUserById(user.getuId());
        AssertUtil.isTrue(null != temp,"用户已存在！");
        AssertUtil.isTrue(userMapper.addUser(user)<1,"用户记录添加失败！");
    }

    //查询用户
    public User getUserById(String uId){
        return userMapper.getUserById(uId);
    }

    //删除用户
    public void deleteUserById(String uId){
        AssertUtil.isTrue(null == uId || null == userMapper.getUserById(uId),"待删除用户不存在!");
        AssertUtil.isTrue(userMapper.deleteUserById(uId)<1,"删除用户失败!");
    }

    //修改用户
    public void updateUserPassword(User user){
        //判断用户ID
        AssertUtil.isTrue(user.getuId() == null,"用户不存在！");
        //通过用户ID查询用户对象
        User temp = userMapper.getUserById(user.getuId());
        //判断用户是否存在
        //修改操作本身存在一条记录，如果要修改的用户名是本身占用的名称，则可用
        //执行操作
        AssertUtil.isTrue(userMapper.updateUserPassword(user) < 1,"修改用户失败！");
    }*/
}
