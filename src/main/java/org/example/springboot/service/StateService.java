package org.example.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.springboot.po.State;

public interface StateService extends IService<State> {
}
