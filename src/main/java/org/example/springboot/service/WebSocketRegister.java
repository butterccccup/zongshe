

package org.example.springboot.service;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint("/websocket/register")
@Component
public class WebSocketRegister {
    public static MessageService M;

    static Log log = LogFactory.getLog(WebSocketRegister.class);
    private static int onlineCount = 0;
    private static CopyOnWriteArraySet<WebSocketRegister> webSocketSet = new CopyOnWriteArraySet();
    private Session session;
    private String sid = "";
    public WebSocketRegister(){

    }

    @OnOpen
    public void onOpen(Session session){
        this.session = session;
        webSocketSet.add(this);
        addOnlineCount();
        log.info("有新窗口开始监听:" + sid + ",当前在线人数为" + getOnlineCount());
        this.sid = sid;
        //M.recvMessage("{'ID':'44:EA:56:3C:8A:E9','TYPE':'logonInfo','TIME':'2021-06-11 22:23:00','CONTENT':{'password':'admin', 'pin':'off'}}");
        //M.recvMessage("{'ID':'04:EA:56:3C:8A:E9','TYPE':'state','TIME':'2021-06-11 22:23:00','CONTENT':{'temperature':'45', 'pin':'off'}}");
        /*try {
            this.sendMessage("连接成功");
            this.sendMessage("JSON");
        } catch (IOException var4) {
            log.error("websocket IO异常");
        }
         */
        try {
            //this.sendMessage("连接成功");
            //this.sendMessage("JSON");
            System.out.println("树莓派连接建立！！！");
        } catch (Exception var4) {
            log.error("websocket IO异常");
        }
    }

    @OnClose
    public void onClose(){
        webSocketSet.remove(this);
        subOnlineCount();
        log.info("有一连接关闭！当前在线人数为" + getOnlineCount());
    }

    @OnMessage
    public void onMessage(String message,Session session) throws IOException{
        System.out.println("hi");
        System.out.println(message);
        this.sendMessage(M.recvMessage(message).toString());
        //System.out.println(UserMapper.queryByUserID("jason"));
        //M.recvMessage("{'ID':'04:EA:56:3C:8A:E9','TYPE':'register','TIME':'2021-06-11 22:23:00','CONTENT':{'temperature':'45', 'pin':'off'}}");
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    public static void sendInfo(String message, @PathParam("sid") String sid) throws IOException {
        log.info("推送消息到窗口" + sid + "，推送内容:" + message);

        for (WebSocketRegister item : webSocketSet) {
            try {
                if (sid == null) {
                    item.sendMessage(message);
                } else if (item.sid.equals(sid)) {
                    item.sendMessage(message);
                }
            } catch (IOException var5) {
            }
        }

    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        ++onlineCount;
    }

    public static synchronized void subOnlineCount() {
        --onlineCount;
    }
}
