package org.example.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.example.springboot.dao.StateMapper;
import org.example.springboot.po.State;
import org.example.springboot.service.StateService;
import org.springframework.stereotype.Service;
@Service
public class StateServiceImpl extends ServiceImpl<StateMapper, State> implements StateService {

}
