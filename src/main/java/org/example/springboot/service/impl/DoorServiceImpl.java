package org.example.springboot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.springboot.dao.DoorMapper;
import org.example.springboot.po.Door;
import org.example.springboot.service.DoorService;
import org.springframework.stereotype.Service;
@Service
public class DoorServiceImpl extends ServiceImpl<DoorMapper, Door> implements DoorService {
}
