package org.example.springboot.service;
import com.alibaba.fastjson.JSON;
import org.example.springboot.Util.logCONTENT;
import org.example.springboot.Util.passwordCONTENT;
import org.example.springboot.Util.stateCONTENT;
import org.example.springboot.po.Door;
import org.example.springboot.po.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
@Component
public class MessageService {
    private String ID;
    private String TYPE;
    private String TIME;
    private String CONTENT;
    @Autowired
    private StateService stateService;
    @Autowired
    private DoorService doorService;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getTIME() {
        return TIME;
    }

    public void setTIME(String TIME) {
        this.TIME = TIME;
    }

    public String getCONTENT() {
        return CONTENT;
    }

    public void setCONTENT(String CONTENT) {
        this.CONTENT = CONTENT;
    }

    public MessageService(){}

    public MessageService(String ID, String TYPE, String TIME, String CONTENT){

        this.ID = ID;
        this.TYPE = TYPE;
        this.TIME = TIME;
        this.CONTENT = CONTENT;
    }
    @Override
    public String toString() {
       /* return "message{" +
                "ID='" + ID + '\'' +
                ", TYPE='" + TYPE + '\'' +
                ", TIME='" + TIME + '\'' +
                ", CONTENT='" + CONTENT + '\'' +
                '}';

        */
        return "{" +
                "\"ID\":\"" + ID + "\"" +
                ", \"TYPE\":\"" + TYPE + "\"" +
                ", \"TIME\":\"" + TIME + "\"" +
                ", \"CONTENT\":" + CONTENT  +
                "}";



    }
//recvMessage应该返回一个字符串，这个字符串才该是给service返回的东西，应该由service来调用util.
    public MessageService recvMessage(String json1){

        Date date=new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime=sdf.format(date);
        MessageService msg = JSON.parseObject(json1, MessageService.class);
        if(msg.TYPE.equals("state")){
            stateCONTENT stateMsg = JSON.parseObject(msg.CONTENT,stateCONTENT.class);
            State state = new State();
            state.setSpin(stateMsg.getPin());
            state.setStemperature(stateMsg.getTemperature());
            state.setStime(nowTime);
            state.setDid(msg.getID());
            if(!stateService.save(state)){
                System.out.println("插入失败！");
            }
            return formMessage(msg.ID,"instruction",nowTime,"{\"description\":\"收到心跳包\"}");
        }else if(msg.TYPE.equals("emergency")) {//紧急信息
            return formMessage(msg.ID,"instruction",nowTime,"{\"description\":\"ring\"}");
        }else if(msg.TYPE.equals("log")){//日志模块
            logCONTENT logMsg = JSON.parseObject(msg.CONTENT,logCONTENT.class);
            return formMessage(msg.ID,"instruction",nowTime,"{\"description\":\"收到日志\"}");
        }else if(msg.TYPE.equals("reply")){
            //打印来自边缘设备的回复信息
            System.out.println(msg.CONTENT);
            return formMessage(msg.ID,"instruction",nowTime,"{\"description\":\"收到reply回复\"}");
        }else if(msg.TYPE.equals("errorInfo")){
            //打印出错信息
            System.out.println(msg.CONTENT);
            return formMessage(msg.ID,"instruction",nowTime,"{\"description\":\"收到错误信息\"}");
        }else if(msg.TYPE.equals("logonInfo")){//登录模块
            Door door = doorService.getById(msg.ID);
            if(!door.isIfregistered())  //设备未注册，无法登录
                return formMessage(msg.ID,msg.TYPE,nowTime,"{\"description\":\"logon unsuccessfully\"}");
            passwordCONTENT passwordCONTENT = JSON.parseObject(msg.CONTENT, passwordCONTENT.class);
            if(door != null && door.getDpassword().equals(passwordCONTENT.getPassword()))
            {
                System.out.println("登录成功！");
                return formMessage(msg.ID, msg.TYPE, nowTime, "{\"description\":\"logon successfully\"}");   //发送给边缘设备的信息
            } else
                return formMessage(msg.ID,msg.TYPE,nowTime,"{\"description\":\"logon unsuccessfully\"}");
        }else if(msg.TYPE.equals("register")){//注册功能
            if(doorService.getById(msg.ID) == null){  //设备一开始不在数据库中
                System.out.println("不允许注册");
                return formMessage(msg.ID,"register",nowTime,"{\"description\":\"register unsuccessfully\"}");
            }else if(doorService.getById(msg.ID) != null && doorService.getById(msg.ID).isIfregistered()){ //设备已经注册了
                System.out.println("设备已注册");
                return formMessage(msg.ID,"register",nowTime,"{\"description\":\"register unsuccessfully\"}");
            } else if(doorService.getById(msg.ID) != null && !doorService.getById(msg.ID).isIfregistered()){  //设备未注册
                Door door = doorService.getById(msg.ID);
                passwordCONTENT passwordCONTENT = JSON.parseObject(msg.CONTENT, passwordCONTENT.class);

                if(door.getDpassword().equals(passwordCONTENT.getPassword())) {
                    door.setIfregistered(true).setDdate(nowTime);
                    if (doorService.updateById(door)) {
                        System.out.println("注册成功");
                        return formMessage(msg.ID, "register", nowTime, "{\"description\":\"register successfully\"}");
                    }
                } else {
                    return formMessage(msg.ID,"register",nowTime,"{\"description\":\"register unsuccessfully\"}");
                }
            }

        }

        return formMessage(msg.ID,"errorInfo",nowTime,"{\"description\":\"解析失败\"}");
    }

    public MessageService formMessage(String ID, String TYPE, String TIME, String CONTENT){
        //此处生成要发送的信息
        MessageService msg = new MessageService(ID,TYPE,TIME,CONTENT);
        //String msgJSON = JSON.toJSONString(msg);
        return msg;
    }


}

