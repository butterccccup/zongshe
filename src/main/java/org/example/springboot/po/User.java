package org.example.springboot.po;

import com.baomidou.mybatisplus.annotation.TableId;

public class User {
    @TableId(value = "uId")
    private String uid;
    private String did;
    private String upassword;
    private String uname;
    private String ubirth;
    private String utel;
    private String uunit;
    private Boolean admin;


    public User() {
    }

    public User(String did, String uid, String upassword, String uname, String ubirth, String utel, String uunit) {
        this.did = did;
        this.uid = uid;
        this.upassword = upassword;
        this.uname = uname;
        this.ubirth = ubirth;
        this.utel = utel;
        this.uunit = uunit;
    }

    public String getUid() {
        return uid;
    }

    public User setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getDid() {
        return did;
    }

    public User setDid(String did) {
        this.did = did;
        return this;
    }

    public String getUpassword() {
        return upassword;
    }

    public User setUpassword(String upassword) {
        this.upassword = upassword;
        return this;
    }

    public String getUname() {
        return uname;
    }

    public User setUname(String uname) {
        this.uname = uname;
        return this;
    }

    public String getUbirth() {
        return ubirth;
    }

    public User setUbirth(String ubirth) {
        this.ubirth = ubirth;
        return this;
    }

    public String getUtel() {
        return utel;
    }

    public User setUtel(String utel) {
        this.utel = utel;
        return this;
    }

    public String getUunit() {
        return uunit;
    }

    public User setUunit(String uunit) {
        this.uunit = uunit;
        return this;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public User setAdmin(Boolean admin) {
        this.admin = admin;
        return this;
    }

}
