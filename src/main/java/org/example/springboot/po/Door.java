package org.example.springboot.po;

import com.baomidou.mybatisplus.annotation.TableId;

public class Door {
    @TableId(value = "did")
    private String did;
    private String dpassword;
    private String ddate;
    private String dunit;
    private boolean ifregistered;

    public boolean isIfregistered() {
        return ifregistered;
    }

    public Door setIfregistered(boolean ifregistered) {
        this.ifregistered = ifregistered;
        return this;
    }

    public String getDid() {
        return did;
    }

    public Door setDid(String did) {
        this.did = did;
        return this;
    }

    public String getDpassword() {
        return dpassword;
    }

    public Door setDpassword(String dpassword) {
        this.dpassword = dpassword;
        return this;
    }

    public String getDdate() {
        return ddate;
    }

    public Door setDdate(String ddate) {
        this.ddate = ddate;
        return this;
    }

    public String getDunit() {
        return dunit;
    }

    public Door setDunit(String dunit) {
        this.dunit = dunit;
        return this;
    }
}
