package org.example.springboot.po;

import com.baomidou.mybatisplus.annotation.TableId;

public class State {
    @TableId(value = "stime")
    private String stime;
    private String did;
    private String stemperature;
    private String spin;

    public String getStime() {
        return stime;
    }

    public State setStime(String stime) {
        this.stime = stime;
        return this;
    }

    public String getDid() {
        return did;
    }

    public State setDid(String did) {
        this.did = did;
        return this;
    }

    public String getStemperature() {
        return stemperature;
    }

    public State setStemperature(String stemperature) {
        this.stemperature = stemperature;
        return this;
    }

    public String getSpin() {
        return spin;
    }

    public State setSpin(String spin) {
        this.spin = spin;
        return this;
    }



}
