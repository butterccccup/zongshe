package org.example.springboot.config;

import org.example.springboot.service.WebSocketServer;
import org.example.springboot.service.MessageService;
import org.example.springboot.service.WebSocketRegister;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.annotation.Resource;

@Configuration
public class WebSocketConfig {
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
    @Resource
    public void setMessageService(MessageService M){
        WebSocketRegister.M = M;
        WebSocketServer.M = M;
    }

}
